<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Controllers\ClienteController;
use App\Http\Controllers\CompraController;
use App\Http\Controllers\ModeloController;
use App\Http\Controllers\ProdutoController;
use App\Http\Controllers\RastreamentoController;
use App\Http\Controllers\RemessaController;
use App\Http\Controllers\ServicoController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VendedorController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Pagina Inicial
Route::get('/', function () {
    return view('landing');
});

Route::get('/dashboard', function () {
    if(Auth::user()->servico == null){
        return redirect()->route('servico.create');
    }else{
        return view('dashboard');
    }
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Route::post('/modelo/process', "App\Http\Controllers\ModeloController@process")->name('modelo.process')->middleware(['auth']);

Route::resource('cliente', ClienteController::class)->middleware(['auth']); //Rotas De Cliente
Route::resource('compra', CompraController::class)->middleware(['auth']); //Rotas De Compra
Route::resource('modelo', ModeloController::class)->middleware(['auth']); //Rotas De Compra
Route::resource('produto', ProdutoController::class)->middleware(['auth']); //Rotas De Produto
Route::resource('rastreamento', RastreamentoController::class)->middleware(['auth']); //Rotas De Rastreamento
Route::resource('remessa', RemessaController::class)->middleware(['auth']); //Rotas De Remessaa
Route::resource('servico', ServicoController::class)->middleware(['auth']); //Rotas De Servico
Route::resource('users', UserController::class)->middleware(['auth']); //Rotas De User
Route::resource('vendedor',VendedorController::class)->middleware(['auth']); //Rotas De Vendedor

//Rota De Geracao De Modelos

//Rotas De Datatable
Route::prefix('/api')->group(function (){ //// endereco/api
    Route::prefix('/datatables')->group(function () { //// endereco/api/datatables
        Route::get('/cliente', "App\Http\Controllers\ClienteController@datatableProvider")->name('datatables.cliente')->middleware(['auth']); //Cliente
        Route::get('/compra', "App\Http\Controllers\CompraController@datatableProvider")->name('datatables.compra')->middleware(['auth']); //Compra
        Route::get('/modelo', "App\Http\Controllers\ModeloController@datatableProvider")->name('datatables.modelo')->middleware(['auth']); //Modelo
        Route::get('/produto', "App\Http\Controllers\ProdutoController@datatableProvider")->name('datatables.produto')->middleware(['auth']); //Produto
        Route::get('/rastreamento', "App\Http\Controllers\RastreamentoController@datatableProvider")->name('datatables.rastreamento')->middleware(['auth']); //Rastreamento
        Route::get('/remessa', "App\Http\Controllers\RemessaController@datatableProvider")->name('datatables.remessa')->middleware(['auth']); //Remessa
        Route::get('/vendedor', "App\Http\Controllers\VendedorController@datatableProvider")->name('datatables.vendedor')->middleware(['auth']); //Vendedor
    });
});

//Rota De Sobre
Route::get('/sobre', function () {
    return view('outros.sobre');
})->name('sobre')->middleware(['auth']);

Route::get('/instrucoes', function () {
    return view('outros.instrucoes');
})->name('instrucoes')->middleware(['auth']);














//Pagina Testes Editor
Route::get('/editor', function () {
    return view('editor');
})->name("editor");

