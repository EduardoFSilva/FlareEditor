<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Servico;
use App\Models\User;
use App\Models\Modelo;
use App\Models\Compra;


class ModeloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $servico = Auth::user()->servico;
        $modelos = $servico->modelo;
        return view("modelo.index",['modelos'=>$modelos]);
    }

    /**
     * Processa o modelo de email(substitui a variavel por seus valores)
     * dado o Id de uma compra e um modelo
     * 
     * @return \Illuminate\Http\Response
     */
    public function process(Request $request)
    {
        //Valida para ver se nenhum dos IDs é nulo
        $request->validate([
            "modeloId"=>['required'],
            'compraId'=>['required']
        ]);
        //Encontra a compra do ID recebido fazendo todos joins necessários
        $compra = Compra::where('id',$request->input('compraId'))->with(['servico','cliente','produto','produto.vendedor','remessa','remessa.rastreamento'])->get()[0];
        //Encontra o modelo do ID recebido
        $modelo = Modelo::find($request->input('modeloId'));
        //Verifica se nenhum dos eloquents é nulo
        if($modelo == null || $compra == null){
            //Se sim, envia um header de redirect back
            return redirect()->back();
        }else{
            //Se não, processa a variavel
            //Recebe o TEXT guardado no campo conteudo do modelo
            $textInput = $modelo->conteudo;
            //Regex usado para busca por variaveis no formato {'variavel'} ou {'variavel.composta'}, ignorando case
            $pattern = "/({'[a-z+\.?]+'})/mi";
            //Regex usado para remover aspas simples e chaves da variavel para que possa ser interpretada pelo dicionario
            $repPatt = "/{|}|'/mi";
            //Array que irá receber as variaveis encontradas pelo preg_match_all
            $matches = []; 
            preg_match_all($pattern,$textInput,$matches);
            //Copia Do Text Input para se realizar substituições
            $proc = $textInput;
            /**
             * Percorre as variaveis encontradas
             * $index - indice do array relativo a variavel
             * $key - variavel encontrada pelo preg_match_all usando Regex
             */
            foreach ($matches[0] as $index => $key) {
                //Removendo as aspas e chaves para buscar no dicionario
                $noVarSignString = preg_replace($repPatt,"",$key);
                /**
                 * Substituindo o valor e sobreescrevendo $proc
                 * str_replace irá substituir a chave pelo valor retornado da função
                 * valorVariavel($chave,$compra) na string $proc
                 */
                $proc = str_replace($key,$this->valorVariavel($noVarSignString,$compra),$proc);
            }
            //Retorna a view com a string com as variaveis substituidas por seus devidos valores
            return view('modelo.process',["parsedHtml"=>$proc]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $servico = Auth::user()->servico;
        $teste = '<b><span style="font-size: 48px;"><font color="#0000ff">EEEEEEEEEÉÉÉÉÉÉE</font></span></b>';
        return view("modelo.create",['servico'=>$servico,"teste"=>$teste])->withFields(['mensagem']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'titulo'=>['required','string','max:255'],
            'descricao'=>['nullable','max:511'],
            'mensagem'=>['required'],
            'servicoId'=>['required']
        ]);
        $modelo = new Modelo();
        $modelo->servico_id = $request->input('servicoId');
        $modelo->conteudo = $request->input('mensagem');
        $modelo->descricao = $request->input('descricao');
        $modelo->titulo = $request->input('titulo');
        $modelo->save();
        $request->session()->flash('mensagemSucessoAdicionar', "Modelo Criado Com Sucesso");
        return redirect()->route("modelo.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $modelo = Modelo::find($id);
        $servico = $modelo->servico;
        if($modelo == null){
            return redirect()->back;
        }else{
            return view('modelo.show',['modelo'=>$modelo,'servico'=>$servico]);
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $modelo = Modelo::find($id);
        $servico = $modelo->servico;
        if($modelo == null){
            return redirect()->back;
        }else{
            return view('modelo.edit',['modelo'=>$modelo,'servico'=>$servico]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $modelo = Modelo::find($id);
        $servico = $modelo->servico;
        if($modelo == null){
            return redirect()->back;
        }else{
            $request->validate([
                'titulo'=>['required','string','max:255'],
                'descricao'=>['nullable','max:511'],
                'mensagem'=>['required'],
                'servicoId'=>['required']
            ]);
            $modelo->servico_id = $request->input('servicoId');
            $modelo->conteudo = $request->input('mensagem');
            $modelo->descricao = $request->input('descricao');
            $modelo->titulo = $request->input('titulo');
            $modelo->save();
            $request->session()->flash('mensagemSucessoAtualizar', "Modelo Editado Com Sucesso");
            return redirect()->route("modelo.index");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $modelo = Modelo::find($id);
        if($modelo == null){
            return redirect()->back();
        }else{
            $tituloModelo = $modelo->titulo."";
            $modelo->delete();
            $request->session()->flash('mensagemSucessoApagar', "Modelo \"".$tituloModelo."\" apagado com sucesso");
            return redirect()->route("modelo.index");
        }
    }

    public function datatableProvider(){
        $servico = Auth::user()->servico;
        $remessa = $servico->modelo;
        return response()->json($remessa);
    }

    /**
     * Função Responsavel Por Traduzir o Eloquent de Compras em valores substituiveis
     * 
     * @return String
     */
    private function valorVariavel($chave,$compra){
        //Dicionario De Tradução de chave para seletor do array
        //As Unicas Excessões A Regra São Nome Completo, Preco Total
        //e Data De Entrega que requerem um processamento adicional
        if($chave == "cliente.nomeCompleto"){
            return $compra->cliente->nome." ".$compra->cliente->sobrenome;
        }else if($chave == "remessa.dataDeEntrega"){
            //Converte a data em Time
            $time = strtotime($compra->remessa->dataDeEntrega);
            //Dicionario De Meses. O mês é seu número - 1. Exemplo: Janeiro index 0, Fevereiro index 1 e assim por diante
            $meses = ["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"];
            //Seleciona o mês
            $mes = $meses[date('m',$time)-1];
            //Escreve <Dia> de <Mes por Extensao> de <Ano>
            $retorno = date('d',$time)." de ".$mes." de ".date('Y',$time);
            return $retorno;
        }else if($chave == "produto.valorTotal"){
            $total = $compra->produto->preco * $compra->quantidade;
            return $total;
        }
        $dicionario = [
            "cliente.primeiroNome" => ["cliente","nome"],
            "cliente.email" => ["cliente","email"],
            "cliente.endereco" => ["cliente","endereco"],
            "produto.descricao" => ["produto","descricao"],
            "produto.preco" => ["produto","preco"],
            "produto.quantidade" => ["quantidade"],
            "produto.link" => ["produto","link"],
            "vendedor.nome" => ['produto',"vendedor","nome"],
            "vendedor.email" => ['produto',"vendedor","email"],
            "vendedor.telefone" => ['produto',"vendedor","telefone"],
            "vendedor.pais" => ['produto',"vendedor","pais"],
            "servico.nome" => ["servico","nomeFantasia"],
            "servico.razao" => ["servico","razaoSocial"],
            "servico.email" => ["servico","email"],
            "servico.telefone" => ["servico","telefone"],
            "rastreamento.telefone" => ['remessa',"rastreamento","telefone"],
            "rastreamento.email" => ['remessa',"rastreamento","email"],
            "rastreamento.site" => ['remessa',"rastreamento","site"],
            "rastreamento.nomeEmpresa" => ['remessa',"rastreamento","nomeEmpresa"],
            "rastreamento.telefone" => ["servico","telefone"],
            "remessa.codigo" => ["remessa","codigoRastreamento"],
            "remessa.linkRastreamento" => ["remessa","linkRastreamento"],
            "remessa.tipoRemessa" => ["remessa","tipoRemessa"],
            "remessa.statusRastreamento" => ["remessa","progressoRastreamento"]
        ];
        $path = $dicionario[$chave];
        if($path != null){
            //Copia o Objeto pois a operação é destrutiva
            $copiaCompra = $compra;
            //Procura Recursivamente Nos Index
            for($index = 0; $index < count($path); $index++){
                $copiaCompra = $copiaCompra[$path[$index]];
            }
            //Retorna Resultado se o dado for diferente de nulo, caso seja nulo retorna uma string vazia
            return ($copiaCompra == null) ? "" : $copiaCompra;
        }
    }
    
}
